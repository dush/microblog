drop table uzytkownik if exists;
create table uzytkownik(
	id int NOT NULL PRIMARY KEY,
	login varchar(20) NOT NULL,
	imie varchar(20) NOT NULL,
	nazwisko varchar(30) NOT NULL,
	emial varchar(30) NOT NULL,
	haslo varchar(30) NOT NULL
);

drop table follower if exists;
create table follower(
	idUzytkownika int NOT NULL,
        idFollowersa int NOT NULL,
        PRIMARY KEY(idUzytkownika, idFollowersa),
        FOREIGN KEY(idUzytkownika) REFERENCES uzytkownik(id),
        FOREIGN KEY(idFollowersa) REFERENCES uzytkownik(id)

);

drop table wpis if exists;
create table wpis(
	id int NOT NULL PRIMARY KEY,
	idUzytkownika int NOT NULL FOREGIN KEY REFERENCES uzytkownik(id),
	tytul varchar(200) NOT NULL,
	tresc varchar(1000),
	czyPubliczny boolean NOT NULL,
	date timestamp
);




