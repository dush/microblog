package pl.wwsis.zpp;

import static spark.Spark.*;

public class App {

public static void main(String[] args) {
	get("/hello", (req, resp)-> "Hello World");
	
	post("/hello", (req, resp)->{
		return req.body();
	});
	
	get("/private", (req, resp)->{
		resp.status(401);
		return "go away!";
	});
	
	get("/users/:name", (req, resp)->{
		return "Selected user" + req.params(":name"); 
	});
	
	get("news/:section", (req,resp)->{
		resp.type("text/html");
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><news><b>" +
			req.params("section") + "</b></news>";
	});
	
	get("/protected", (req, resp)->{
		halt(403, "I don't think so!!!");
		return null;
	});
	
	get("/redirect", (req, resp)->{
		resp.redirect("/news/world");
		return null;
	});
	
	get("/", (req, resp)->{
		return "root";
	});
}

}