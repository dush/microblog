package pl.wwsis.zpp;

import static spark.Spark.*;

import java.util.HashMap;
import java.util.Map;

import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;

public class MicroBlogApp {

	public static void main(String[] args) {
		get("/*", (req, res) -> {
			Map<String, Object> attributes = new HashMap<>();
			attributes.put("message", "HelloWorld!");
			attributes.put("equation", "2+3");
			attributes.put("sumVal", 2+3 );
			return new ModelAndView(attributes,"hello.ftl");
		}, new FreeMarkerEngine());
	}

}
